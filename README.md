# Getränkekarten-Update-System

Zum Ausführen des Scripts: python3.5 script.py

Ein Python skript gedacht um Getränkekarten ändern zu können ohne auf
den Tisch klettern zu müssen.

Das Script läuft in einem endlos loop und prüft, ob der git server eine neue
Version der Karte hat. Falls ja wird diese gepullt und zum PDF kompilliert.
Das PDF sollte in einem viewer angezeigt werden, der bei Änderungen in der
angezeigten Datei automatisch aktualisiert.

Das Script muss innerhalb des lokalen repositorys ausgeführt werden und
benötigt Python 3.5 und eine stabile Internetverbindung. Delays, Dateinamen
und latex engines müssen gegebenenfalls angepasst werden.
