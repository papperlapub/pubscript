#!/usr/bin/env python3.5

print("\nStarting script...\n")

#from subprocess import run, Popen, PIPE
from subprocess import Popen, PIPE, run
from time import sleep

# Endless loop - Program is terminated manually using ctrl-C
while(True):

    # Opens a subprocess and executes "git fetch". The output is redirected using PIPE
    p = Popen(["git", "fetch"], stdout=PIPE, stderr=PIPE)

    # With this line we extract the output (stdout and stderr respectively)
    out, err = p.communicate()

    # If fetch finds something, it will write to stderr, so we check if that stream is empty
    if not (err == b''):

        # If we found a new version in the remote, we pull the changes
        run(["git", "pull"])

        # Then we compile the file into a pdf. Filename and latex engine can be changes as necessary
        run(["xelatex", "PubCard.tex"])

    # Then we sleep for some time - That little delay won't be too bad.
    sleep(1)
